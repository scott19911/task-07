package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

	private static DBManager instance = null;
	private Connection connection;


	public static synchronized DBManager getInstance() {
		if(instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
//		try {
//			Class.forName("com.mysql.cj.jdbc.Driver");
//		} catch (ClassNotFoundException e) {
//			System.out.println("Class.forName load status: FAIL !");
//			e.printStackTrace();
//
//		}
		try {
			connection= DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
		} catch (SQLException e) {
			System.out.println("Connection to DB status= fail!");
			e.printStackTrace();
		}
	}
	Connection getConnection()  {

		return connection;
	}

	public List<User> findAllUsers() throws DBException {

		Statement statement;
		ResultSet rs;
		List<User> list = new ArrayList<>();
		try {
			statement = getConnection().createStatement();
			rs = statement.executeQuery(Querys.SELECT_ALL_USERS.getSQL());
			while (rs.next()) {
				int id = rs.getInt("id");
				String login = rs.getString("login");
				User user = new User();
				user.setLogin(login);
				user.setId(id);
				list.add(user);
			}
		} catch (SQLException e) {
			//throw new DBException("Fail findAllUsers",e);
		}

		return list;
	}

	public boolean insertUser(User user) throws DBException {

		Statement statement ;

		try {
			statement = connection.createStatement();
			statement.executeUpdate(Querys.INSERT_USER_BY_LOGIN.getSQL()
					.replace("?", user.getLogin()), Statement.RETURN_GENERATED_KEYS);

				ResultSet rs = statement.getGeneratedKeys();

				if (rs != null && rs.next()) {
					user.setId(rs.getInt(1));
				}
				System.out.println(user.getLogin() + " " + user.getId());
				return true;

		} catch (SQLException e) {
			//throw new DBException("Fail insertUser" + user.getLogin(),e);
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {

		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(Querys.DELETE_USERS_BY_LOGIN.getSQL());
			for (int i = 0; i < users.length ; i++){
				statement.setString(1,users[i].getLogin());
				statement.execute();

			}
			return true;
		} catch (SQLException e) {
			//throw new DBException("Fail deleteUsers" ,e);
		}
		return false;
	}

	public User getUser(String login) throws DBException {

		PreparedStatement statement;
		User user = new User();
		try {
			statement = connection.prepareStatement(Querys.GET_USER_BY_LOGIN.getSQL());
			statement.setString(1,login);
			ResultSet rs = statement.executeQuery();

			while (rs.next()){
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
			}

		} catch (SQLException e) {
			//throw new DBException("Fail getUser" + user.getLogin() ,e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {

		PreparedStatement statement;
		Team team = new Team();

		try {
			statement = connection.prepareStatement(Querys.GET_TEAM_BY_NAME.getSQL());
			statement.setString(1, name);
			ResultSet rs = statement.executeQuery();
			while (rs.next()){
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
			}
		} catch (SQLException e) {
		//	throw new DBException("Fail getTeam" + name,e);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		Statement statement;
		ResultSet rs;
		List<Team> list = new ArrayList<>();

		try {
			statement = getConnection().createStatement();
			rs = statement.executeQuery(Querys.SELECT_ALL_TEAM.getSQL());

			while (rs.next()){
				int id = rs.getInt("id");
				String name = rs.getString("name");
				Team team =	new Team();
				team.setName(name);
				team.setId(id);
				list.add(team);
			}

		} catch (SQLException e) {
			//throw new DBException("Fail findAllTeams",e);
		}

		return list;
	}

	public boolean insertTeam(Team team) throws DBException {

		Statement statement ;

		try {
			statement = connection.createStatement();
			statement.executeUpdate(Querys.INSERT_TEAM.getSQL()
					.replace("?",team.getName()), PreparedStatement.RETURN_GENERATED_KEYS);
			ResultSet rs = statement.getGeneratedKeys();

			if (rs.next()) {

				team.setId(rs.getInt(1));
			}

			return true;
		} catch (SQLException e) {
		//	throw new DBException("Fail insertTeam" + team.getName(),e);
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {


		PreparedStatement statement;
		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println(user.getId() + " " + user.getLogin());
		try {
			statement = connection.prepareStatement(Querys.INSERT_USERS_TEAM_TABLE.getSQL());

			for (Team team : teams) {
				statement.setInt(1, getUser(user.getLogin()).getId());
				statement.setInt(2, team.getId());
				statement.executeUpdate();
			}

			connection.commit();
			connection.setAutoCommit(true);
			return true;
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException("Fail setTeamsForUser" + user.getLogin(),e);

		}

		//return false;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		PreparedStatement statement ;
		ResultSet rs;

		try {
			statement = connection.prepareStatement(Querys.GET_USER_TEAM.getSQL());
			statement.setString(1,user.getLogin());
			rs = statement.executeQuery();

			while (rs.next()){
				teams.add(getTeam(rs.getString("name")));
			}

		} catch (SQLException e) {
			//throw new DBException("Fail getUserTeams" + user.getLogin(),e);
		}
		Team t = new Team();

		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {

		PreparedStatement statement ;
		try {
			statement = connection.prepareStatement(Querys.DELET_TEAM_BY_NAME.getSQL());
			statement.setString(1,team.getName());
			return statement.execute();
		} catch (SQLException e) {
			//throw new DBException("Fail deleteTeam" + team.getName(),e);
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {

		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(Querys.UPDATE_TEAM.getSQL());
			statement.setInt(2, team.getId());
			statement.setString(1, team.getName());
			return statement.execute();
		} catch (SQLException e) {
			//throw new DBException("Fail updateTeam" + team.getName(),e);
		}
		return false;

	}

}
