package com.epam.rd.java.basic.task7.db.entity;

public enum Querys {
    SELECT_ALL_USERS("select id, login from users"),
    INSERT_USER_BY_LOGIN("insert into users (login) values ('?')"),
    DELETE_USERS_BY_LOGIN("delete from users where login =? "),
    GET_USER_BY_LOGIN("select id, login from users where login = ?"),
    GET_TEAM_BY_NAME("select id, name from teams where name = ?"),
    SELECT_ALL_TEAM("select * from teams"),
    INSERT_TEAM("insert into teams (name) values ('?')"),
    INSERT_USERS_TEAM_TABLE("insert into users_teams values (?,?) "),
    GET_USER_TEAM("select t.name, t.id from users_teams "+
            "inner join teams as t on users_teams.team_id = t.id "+
            "left join users as u on users_teams.user_id = u.id "+
            "where u.login= ? "),
    DELET_TEAM_BY_NAME("delete from teams where  name= ? "),
    UPDATE_TEAM("update teams SET name = ? where id= ?");
    private final String sql;

    Querys(String sql) {
        this.sql = sql;
    }

    public String getSQL() {
        return sql;
    }
}
