package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		Team team = new Team();
		team.setId(0);
		team.setName(name);
		return team;
	}
	@Override
	public boolean equals(Object obj) {
		return this.getName().equals(obj.toString());
	}
	@Override
	public String toString(){
		return this.getName();
	}
}
