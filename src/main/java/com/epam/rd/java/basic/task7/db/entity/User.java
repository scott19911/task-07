package com.epam.rd.java.basic.task7.db.entity;

import com.epam.rd.java.basic.task7.db.DBManager;

import java.util.Objects;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
	User user = new User();
	user.setId(0);
	user.setLogin(login);

		return user;
	}
	@Override
	public boolean equals(Object obj) {
		return (this.getLogin().equals(obj.toString()));
	}
	@Override
	public String toString(){
		return this.getLogin();
	}
}
